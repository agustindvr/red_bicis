let Bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
  return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
  Bicicleta.allBicis.push(aBici);
}


Bicicleta.findById = function(aBiciId){
  var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
  if(aBici)
    return aBici;
  else
    throw new Error(`No existe una bicicleta con el id ${aBiciId}`)
}

Bicicleta.removeById = function (aBiciId){
  for(var i = 0; i < Bicicleta.allBicis.length ; i++){
    if(Bicicleta.allBicis[i].id == aBiciId){
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
}

/*
let a = new Bicicleta(1, 'Rojo', 'BMX Dirt Jump', [-34.6012424,-58.3861497]);
let b = new Bicicleta(2, 'Blanca', 'BMX Park', [-34.596932, -58.3808287]);
let c = new Bicicleta(3, 'Fuego', 'Street BMX', [-34.670228, -58.365313]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
*/

module.exports = Bicicleta;