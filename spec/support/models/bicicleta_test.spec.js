let Bicicleta = require('../../../models/bicicleta')


beforeEach(() => {Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () => {
  it('comienza vacia', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  })
})

describe('Bicicleta.add', () => {
  it('agregamos una', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    let a = new Bicicleta(1, 'Rojo', 'BMX Dirt Jump', [-34.6012424, -58.3861497]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);

  })
})


describe('Bicicleta.findByID', () => {
  it('debe devolver la bici co id 1', () =>{

    expect(Bicicleta.allBicis.length).toBe(0);
    let aBici = new Bicicleta(1, "verde", "urbana");
    let aBici2 = new Bicicleta(2, "azul", "campo");
    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);

    let targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);
  })
});

describe('Bicicleta.removeByID', () => {
  it('removemos una', () => {
    
    expect(Bicicleta.allBicis.length).toBe(0);
    let r = new Bicicleta(1, "verde", "urbana");
    Bicicleta.removeById(r);

  })
})